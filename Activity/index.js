// alert("Hi World")

// MINI_ACTIVITY
/*
1. Using the ES6 update, get the cube of 8.
2. Print the result on the console with the message: 'The interest rate on your savings account is' + result
3. Use the Template Literal in printing the message.
*/

let cubeNum = 8 ** 3;
console.log(`The cube of 8 is: ${cubeNum}`);

// MINI-ACTIVITY
/*
1. Destructure the address array
2. print the values in console: I live at 258 Washington Vaenue, California, 99011
3. Use template literals
4. Send the output on hangouts
*/
const address = ["258", "Washington Ave NW", "California", "90011"]

const [houseNo, street, state, zipCode] = address;

console.log(`I live at ${houseNo} ${street}, ${state}, ${zipCode}`);

// MINI-ACTIVITY
/*
1. Destructure the animal array
2. Print the values in the console: 'Lolong was a saltwater crocodile. He weighed at 1075 kgs with a measurement of 20ft 3 in'
3. Use template literals
4. Send the out on hangouts
*/
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kg",
	measurement: "20ft 3 in"
}

const { name, species, weight, measurement } = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`)

// MINI-ACTIVITY
/*
1. Loop through the numbers using forEach using arrow function
2. Print the numbers in the console
3. Use the .reduce operator on the numbers array
4. Assign the result on a variable
5. Print the variable on the console
*/

let numbers = [1, 2, 3, 4, 5];

const loop = () => {}

numbers.forEach(num => console.log(num));


let sum = numbers.reduce((x, y) => x + y);

console.log(sum);

// MINI-ACTIVTY
/*
1. Create a "dog" class
2. Inside of the class "dog", have a name, age, and breed
3. Instantiate a new dog class and print in the console
4. Send the screenshot of the output on hangouts
*/

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let newDog = new Dog('Rusty', 14, 'Azkal');
console.log(newDog);